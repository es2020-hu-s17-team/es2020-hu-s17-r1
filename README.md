
# EuroSkills Graz 2020 nemzeti válogató, Skill 17 (Web Development)


## 1. forduló

Az alábbiakban a válogató első fordulójára vonatkozó legfontosabb információkat osztjuk meg.
### Általános tudnivalók
Ebben a fordulóban egy JavaScript alapú játékot kell elkészítened. Az otthoni feladat megoldása során tetszőleges felkészülési forrásokat használhatsz fel, de a feladatot önállóan kell elvégezned, az elkészült munkának a te képességeidet és tudásodat kell tükröznie.

A beküldött feladat értékelését követően a legjobb 10 versenyzővel személyes interjú keretében beszélgetünk el. Ezt követően 6 versenyzőt választunk ki, ők mérkőzhetnek majd meg egymással az egynapos középdöntőben. (A középdöntő várható időpontja 2019.02.22, péntek.) 

#### A feladat beadásának módja és határideje:

A feladatot kétféle formában kell beadnod:

1.	Forráskód BitBucket repo-ban

	A feladatok beadásához a megoldásod forráskódját tartalmazó BitBucket repository-t (a továbbiakban repo) kell majd megosztanod velünk. YY az a kétszámjegyű személyes kód, amit e-mailben küldtünk ki számodra. Amennyiben nem használtad még a BitBucket szolgáltatást, akkor regisztrálj egy azonosítót a https://bitbucket.org címen, majd hozz létre egy privát repo-t es2020-r1-YY néven, ahol YY a tőlünk e-mailben kapott kétszámjegyű sorszámod. 

	Ha nem használtál még git-et egyáltalán, akkor legfőbb ideje, hogy megismerkedj vele. Nem egy ördöglakat, és most nem is lesz még szükséged rá, hogy nagyon elmélyülj benne. Elég, ha alapszinten megérted a működését, és az alapfunkciók használatával az elkészült munkádat feltöltöd a létrehozott repo-ba. 

	Ezután a megoldásodat tartalmazó repo-t meg kell osztanod velünk. Ehhez a es2020s17@http-foundation.hu címünket használd. Elegendő, ha READ jogosultságot kapunk.

2.	Netlify szolgáltatás segítségével publikált működő játék

	
	- A Netlify egy olyan ingyenesen is használható szolgáltatás, ahol modern weblapok oszthatók meg a nyilvánossággal. A szolgáltatás úgy működik, hogy a Netlify-on létrehozott webalkalmazást egy GIT repoval (pl. BitBucket, GitHub) tudod összekapcsolni. (Vigyázz, csúnya, magyartalan mondat következik! :))  Amikor egy új  commitot push-olsz a központi repoba, akkor a Netlify ennek alapján elkészít egy új buildet, és azt azonnal ki is publikálja az app-hoz tartozó nyilvános címen. Ha csak natív HTML/CSS/JavaScript hármast használsz, akkor a build gyakorlatilag meg fog egyezni a forráskóddal, ha viszont valamilyen JS keretrendszerrel fejlesztettél (pl. React), akkor a rendszer minden szükséges műveletet elvégez, ami a publikálható formára hozáshoz szükséges. Ne ijedj meg, a dolog sokkal egyszerűbb, mint amilyennek elsőre látszik!

	- Regisztrálj egy azonosítót a Netlify-on (https://www.netlify.com/), hozz létre egy új appot az aknakereső játékod számára, válassz egy tetszőleges szabad hostnevet és kapcsold össze az appot a BitBucket repoddal.
 
	- A választott hostnevet (amin a működő játékodat elérjük) írd be a repo README file-jának első sorába. A második sorba írd be a kapott azonosítódat, a harmadikba pedig a nevedet.

#### A feladat

A feladat leírását ebben a repoban, a ES2020-HU-S17-R1_Minesweeper v1_0.pdf fájlban található.

#### Határidő

A feladatok elkészítésének végső határideje: **2019.02.12., kedd, éjfél**
Beadott megoldásnak azt tekintjük, amit a fenti időben a repodban és a netlify-os oldaladon találunk.

Ha kérdésed van, az es2020s17@http-foundation.hu címre írhatsz nekünk.